package com.application.hengleasing.myheng.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.application.hengleasing.myheng.R;
import com.application.hengleasing.myheng.api.ApiClient;
import com.application.hengleasing.myheng.api.ApiInterface;
import com.application.hengleasing.myheng.functions.ListAdapter;
import com.application.hengleasing.myheng.functions.LogOut;
import com.application.hengleasing.myheng.functions.TokenSession;
import com.application.hengleasing.myheng.model.PaymentData;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailLeasingActivity extends AppCompatActivity {

    private String token;
    private String contractNumber;

    private TextView tvPaymentDate;
    private TextView tvBalancePrice;
    private TextView tvPaymentPrice;
    private TextView tvTopayment;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leasing_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        InitInstances();
    }


    private void InitInstances() {

        ListView listView = (ListView) findViewById(R.id.listviews);
        tvPaymentDate = (TextView) findViewById(R.id.txtDate);
        tvBalancePrice = (TextView) findViewById(R.id.tvBalance);
        tvPaymentPrice = (TextView) findViewById(R.id.tvPayment);
        tvTopayment = (TextView) findViewById(R.id.numMoney);

        if (CheckTokenEmpty()) return;

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            contractNumber = extras.getString("SY_NO");
        }

        //Toast.makeText(DetailLeasingActivity.this, " Con " + contractNumber, Toast.LENGTH_LONG).show();


        CallApi(listView, contractNumber);

//        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
//                Settings.Secure.ANDROID_ID);
//
//        Log.e("TOKEN", android_id);
    }

    private boolean CheckTokenEmpty() {
        TokenSession tokenSession = new TokenSession(DetailLeasingActivity.this);
        String token = tokenSession.GetToken();
        if (token.isEmpty()) {
            Toast.makeText(DetailLeasingActivity.this, "Pleas LogIn Again !", Toast.LENGTH_SHORT).show();
            LogOut logOut = new LogOut(DetailLeasingActivity.this, DetailLeasingActivity.this);
            logOut.ProcessLogOut();
            return true;
        }
        Log.e("tttt", token);
        return false;
    }

    private void SetShowTable(ListView listView, String[] dataArray ,int countPaid) {
        // Inflate header view
        ViewGroup headerView = (ViewGroup) getLayoutInflater().inflate(R.layout.activity_header_table, listView, false);
        // Add header view to the ListView
        listView.addHeaderView(headerView);
        // Get the string array defined in strings.xml file
        String[] items = dataArray;
        // Create an adapter to bind data to the ListView
        ListAdapter adapter = new ListAdapter(DetailLeasingActivity.this, R.layout.activity_row_table, R.id.txtPeriods, items ,countPaid);
        // Bind data to the ListView

        // Log.e("dataapi",items[0] );


        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.Logout:
                LogOut logOut = new LogOut(DetailLeasingActivity.this, DetailLeasingActivity.this);
                logOut.ProcessLogOut();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void CallApi(final ListView listView, final String contractNumber) {


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        //Call <UserResponse> call = apiService.getStatusLogIn(username,password);
        Call<List<PaymentData>> call = apiService.getPaymentData(contractNumber);

        call.enqueue(new Callback<List<PaymentData>>()

        {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            String dateNowStr = dateFormat.format(date);


            @Override
            public void onResponse(Call<List<PaymentData>> call, Response<List<PaymentData>> response) {

                List<PaymentData> paymentDatas = response.body();

                Toast.makeText(DetailLeasingActivity.this, "API Success!" + response.code(), Toast.LENGTH_SHORT).show();

                if (!response.isSuccessful() || paymentDatas.isEmpty()) {
                    Toast.makeText(DetailLeasingActivity.this, " Emtpy!", Toast.LENGTH_SHORT).show();
                    return;
                }

                String[] reformattedStr = new String[paymentDatas.size()];
                String[] dataArray = new String[paymentDatas.size()];
                double totalPrice = 0;
                double totalToPayment = 0;
                double totalPaymented = 0;
                double currentToPay = 0;
                double balance = 0;
                int countPaid = 0;
                int countLate = 0;
                int nextMonth = paymentDatas.size() - 1;
                String paymentDate = null;


                SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");


                try {
                    for (int i = 0; i < paymentDatas.size(); i++) {
                        // Log.d("api", Users.get(i).getId() + " " + Users.get(i).getLogin() + " " + Users.size());

                        try {
                            reformattedStr[i] = myFormat.format(fromUser.parse(paymentDatas.get(i).getContractDate()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Date dateNow = format.parse(dateNowStr);
                        Date datePayment = format.parse(reformattedStr[i]);
                        /// Late
                        if ((paymentDatas.get(i).getSlipDate().equals(null) || paymentDatas.get(i).getSlipAmout().equals("0.00"))
                                && dateNow.after(datePayment)) {
                            Log.d("dataapi", " Late " + i + " " + paymentDatas.get(i).getSlipDate() + paymentDatas.get(i).getSlipAmout());

                            countLate++;
                            totalToPayment += Double.parseDouble(paymentDatas.get(i).getCantractPrice().replace(",", ""));
                            nextMonth = i + 1;
                        }
                        /// Paid
                        if ((!paymentDatas.get(i).getSlipDate().equals(null) || !paymentDatas.get(i).getSlipAmout().equals("0.00"))
                                && dateNow.after(datePayment)) {

                            countPaid++;
                            Log.d("dataapi", " Paid "+ countPaid + " " + i + " " + paymentDatas.get(i).getSlipDate() + paymentDatas.get(i).getSlipAmout());


                            totalPaymented += Double.parseDouble(paymentDatas.get(i).getCantractPrice().replace(",", ""));

                        }else {
                            dataArray[i] =  " __ __ ";
                        }

                        // Last loop
                        if(i == paymentDatas.size() - 1 ) {
                            paymentDate = reformattedStr[nextMonth];
                            currentToPay = Double.parseDouble(paymentDatas.get(nextMonth).getCantractPrice().replace(",", ""));
                            totalToPayment += currentToPay;
                            countPaid = countPaid - countLate;
                        }

                        //// Price All
                        totalPrice += Double.parseDouble(paymentDatas.get(i).getCantractPrice().replace(",", ""));

                        if (countPaid == paymentDatas.size()) {
                            paymentDate = "--/--/----";
                            currentToPay = 0.00;
                            totalToPayment = 0.00;
                        }

                        dataArray[i] = paymentDatas.get(i).getContractMonth() + "__" + reformattedStr[i] + "__"
                                + paymentDatas.get(i).getCantractPrice() + " บาท";
                         Log.d("dataapi", dataArray[i]);
                    }
                } catch (Exception e) {
                    Log.e("Error", "There is an error" + e);
                    //Toast.makeText(MyListActivity.this, "Error" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
//                ShowData(Users);
                SetShowTable(listView, dataArray,countPaid);

                balance = totalPrice - ( totalPaymented - currentToPay )   ;
                Log.d("dataapi", countPaid + " " + balance + " " + totalPrice + " : " + totalPaymented + " : " + totalToPayment + " " + paymentDate);


                SetDataVariable(totalToPayment, currentToPay, balance, paymentDate);
            }

            private void SetDataVariable(double totalToPayment, double currentToPay, double balance, String paymentDate) {
                DecimalFormat decimalFormat = new DecimalFormat("0.##");
                tvTopayment.setText(decimalFormat.format(currentToPay) + "  บาท");
                tvPaymentDate.setText(paymentDate);
                tvBalancePrice.setText(decimalFormat.format(balance) + "  บาท");
                tvPaymentPrice.setText(decimalFormat.format(totalToPayment) + "  บาท");
            }

            @Override
            public void onFailure(Call<List<PaymentData>> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(DetailLeasingActivity.this, "API Fail" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.e("api", t.toString());
            }
        });

    }
}
