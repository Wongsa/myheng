package com.application.hengleasing.myheng.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.application.hengleasing.myheng.R;
import com.application.hengleasing.myheng.api.ApiClient;
import com.application.hengleasing.myheng.api.ApiInterface;
import com.application.hengleasing.myheng.functions.LogOut;
import com.application.hengleasing.myheng.functions.TokenSession;
import com.application.hengleasing.myheng.model.ContractData;
import com.application.hengleasing.myheng.model.UserData;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyListActivity extends AppCompatActivity {

    private ListView listView;
    private List<HashMap<String, String>> AndroidMapList = new ArrayList<>();
    private TextView tvName;
    private TextView tvAddress;
    private ImageView imvProfile;

    private static final String KEY_CONTRACT = "ContractNumber";
    private static final String KEY_LICENSE = "LicensePlate";
    private static final String KEY_CARTYPE = "CarType";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initInstances();

    }

    private void initInstances() {

        listView = (ListView) findViewById(R.id.list);
        tvName = (TextView) findViewById(R.id.txtName);
        tvAddress = (TextView) findViewById(R.id.txtAdress);
        imvProfile = (ImageView) findViewById(R.id.imageView);

        if (CheckTokenEmpty())
            return;

        String cardNumber = GetCardNumberFromLogIn();

        CallApiUserData(cardNumber);

        CallApiContractData(cardNumber);

    }

    private String GetCardNumberFromLogIn() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null)
            return extras.getString("CardNumber");
        return "";
    }

    private boolean CheckTokenEmpty() {
        TokenSession tokenSession = new TokenSession(MyListActivity.this);
        String token = tokenSession.GetToken();
        if (token.isEmpty()) {
            Toast.makeText(MyListActivity.this, "Pleas LogIn Again !", Toast.LENGTH_SHORT).show();
            LogOut logOut = new LogOut(MyListActivity.this, MyListActivity.this);
            logOut.ProcessLogOut();
            return true;
        }
        Log.e("tttt", token);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.Logout:
                LogOut logOut = new LogOut(MyListActivity.this, MyListActivity.this);
                logOut.ProcessLogOut();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void SetDataProfile(String prefix, String fullName, String lastName, String nickname) {
        tvName.setText(prefix + " " + fullName + " " + lastName + "  ( " + nickname + " )");
        tvAddress.setText(" - - - - - ");

    }


    private void ShowData(final List<ContractData> contractDatas) {

        for (ContractData android : contractDatas) {

            HashMap<String, String> map = new HashMap<>();

            map.put(KEY_CONTRACT, "เลขที่สัญญา  : " + android.getContractNumber());
            map.put(KEY_LICENSE, "ทะเบียนรถ  : " + android.getCarNumber());
            map.put(KEY_CARTYPE, "ยี่ห้อรถ  : " + android.getCarName());

            AndroidMapList.add(map);
        }

        LoadListView();


        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                SentVariableToDetailPage(itemPosition);
                // Show Alert
                //Toast.makeText(MyListActivity.this, AndroidMapList.get(itemPosition).get(KEY_CONTRACT), Toast.LENGTH_LONG).show();
            }

            private void SentVariableToDetailPage(int itemPosition) {
                Intent intent = new Intent(MyListActivity.this, DetailLeasingActivity.class);
//                intent.putExtra("SY_NO", AndroidMapList.get(itemPosition).get(KEY_CONTRACT));
                intent.putExtra("SY_NO", contractDatas.get(itemPosition).getContractNumber());
                startActivity(intent);
            }
        });
    }

    private void LoadListView() {

        ListAdapter adapter = new SimpleAdapter(MyListActivity.this, AndroidMapList, R.layout.list_item,
                new String[]{KEY_CONTRACT, KEY_CARTYPE, KEY_LICENSE},
                new int[]{R.id.version, R.id.name, R.id.api});

        // Assign adapter to ListView
        listView.setAdapter(adapter);
    }

    public void CallApiContractData(String cardNumber) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        //Call <UserResponse> call = apiService.getStatusLogIn(username,password);
        Call<List<ContractData>> call = apiService.getContractData(cardNumber);

        call.enqueue(new Callback<List<ContractData>>()

        {

            @Override
            public void onResponse(Call<List<ContractData>> call, Response<List<ContractData>> response) {

                List<ContractData> contractDatas = response.body();

                Toast.makeText(MyListActivity.this, "API Success!" + response.code(), Toast.LENGTH_SHORT).show();

                if (!response.isSuccessful() || contractDatas.isEmpty())
                    return;
                //Toast.makeText(MyListActivity.this, "Success!", Toast.LENGTH_SHORT).show();


                try {
                    for (int i = 0; i < contractDatas.size(); i++) {
                        //Log.d("api", leasingDatas.get(i).getId() + " " + leasingDatas.get(i).getLogin() + " " + Users.size());
                        //Toast.makeText(MyListActivity.this, "Show Data " +  leasingDatas.get(i).getLeasingNumber(), Toast.LENGTH_SHORT).show();
                        // values[i] = Users.get(i).getTitle();
                    }
                } catch (Exception e) {
                    Log.e("Error", "There is an error" + e);
                    //Toast.makeText(MyListActivity.this, "Error" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                ShowData(contractDatas);

            }

            @Override
            public void onFailure(Call<List<ContractData>> call, Throwable t) {
                // Log error here since request failed

                Toast.makeText(MyListActivity.this, "API Fail" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.e("api", t.toString());
            }
        });
    }

    public void CallApiUserData(final String cardNumber) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<List<UserData>> call = apiService.getUserData(cardNumber);

        call.enqueue(new Callback<List<UserData>>()

        {

            @Override
            public void onResponse(Call<List<UserData>> call, Response<List<UserData>> response) {

                String prefix = null;
                String fullname = null;
                String lastname = null;
                String nickname = null;

                List<UserData> userDatas = response.body();

                //Toast.makeText(MyListActivity.this, "API Success!" + response.code() + cardNumber, Toast.LENGTH_SHORT).show();


                if (!response.isSuccessful() || userDatas.isEmpty())
                    return;

                try {
                    for (int i = 0; i < userDatas.size(); i++) {
                        //Toast.makeText(MyListActivity.this, "Show Data " +  userDatas.get(i).getFullName()+
                        //        " " + userDatas.get(i).getLastName()  +" "+ userDatas.get(i).getNickname(), Toast.LENGTH_SHORT).show();
                        prefix = userDatas.get(i).getPrefixName();
                        fullname = userDatas.get(i).getFullName();
                        lastname = userDatas.get(i).getLastName();
                        nickname = userDatas.get(i).getNickname();
                    }
                } catch (Exception e) {
                    Log.e("Error", "There is an error" + e);
                    //Toast.makeText(MyListActivity.this, "Error" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                SetDataProfile(prefix, fullname, lastname, nickname);

            }

            @Override
            public void onFailure(Call<List<UserData>> call, Throwable t) {
                // Log error here since request failed

                Toast.makeText(MyListActivity.this, "API Fail" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.e("api", t.toString());
            }
        });
    }
}
