package com.application.hengleasing.myheng.functions;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.application.hengleasing.myheng.R;

/**
 * Created by supanee.wo on 8/8/2560.
 */

public class ListAdapter extends ArrayAdapter<String> {
    int vg;
    String[] item_list;
    Context context;
    int countPaid;


    public ListAdapter(Context context, int vg, int id, String[] item_list, int countPaid) {
        super(context, vg, id, item_list);
        this.context = context;
        this.item_list = item_list;
        this.vg = vg;
        this.countPaid = countPaid;
    }

    static class ViewHolder {
        public TextView txtPeriods;
        public TextView txtDatePeriods;
        public TextView txtMoney;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(vg, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.txtPeriods = (TextView) rowView.findViewById(R.id.txtPeriods);
            holder.txtDatePeriods = (TextView) rowView.findViewById(R.id.txtDatePeriods);
            holder.txtMoney = (TextView) rowView.findViewById(R.id.txtMoney);
            rowView.setTag(holder);
        }

        String[] items = item_list[position].split("__");
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.txtPeriods.setText(items[0]);
        holder.txtDatePeriods.setText(items[1]);
        holder.txtMoney.setText(items[2]);


        if(Integer.parseInt(items[0]) <= countPaid) {
            holder.txtPeriods.setTextColor(Color.rgb(00,205 ,102));
            holder.txtDatePeriods.setTextColor(Color.rgb(00,205 ,102));
            holder.txtMoney.setTextColor(Color.rgb(00,205 ,102));
        }else {
            holder.txtPeriods.setTextColor(Color.RED);
            holder.txtDatePeriods.setTextColor(Color.RED);
            holder.txtMoney.setTextColor(Color.RED);
        }
        return rowView;
    }
}
