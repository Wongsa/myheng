package com.application.hengleasing.myheng.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chatchai.ti on 11/8/2560.
 */

public class ContractData {

    @SerializedName("id")
    private String id;
    @SerializedName("CM_CARD_NO")
    private String cardNumber;
    @SerializedName("SY_NO")
    private String contractNumber;
    @SerializedName("CAR_REGS_NO")
    private String carNumber;
    @SerializedName("MD_NAME")
    private String carName;

    public ContractData(String id, String cardNumber, String contractNumber ,String carNumber,String carName) {
        this.id = id;
        this.cardNumber = cardNumber;
        this.contractNumber = contractNumber;
        this.carNumber = carNumber;
        this.carName = carName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String username) {
        this.contractNumber = contractNumber;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

}
