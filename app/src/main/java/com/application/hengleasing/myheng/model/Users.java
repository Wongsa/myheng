package com.application.hengleasing.myheng.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chatchai.ti on 2/8/2560.
 */

public class Users {
    @SerializedName("login")
    private String login;
    @SerializedName("id")
    private String id;
    @SerializedName("type")
    private String type;
    @SerializedName("site_admin")
    private String siteAdmin;

    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("gravatar_id")
    private String gravatarId;
    @SerializedName("url")
    private String url;
    @SerializedName("html_url")
    private String htmlUrl;
    @SerializedName("followers_url")
    private String followersUrl;
    @SerializedName("following_url")
    private String followingUrl;
    @SerializedName("gists_url")
    private String gistsUrl;

    @SerializedName("starred_url")
    private String starredUrl;
    @SerializedName("subscriptions_url")
    private String subscriptionsUrl;
    @SerializedName("organizations_url")
    private String organizationsUrl;
    @SerializedName("repos_url")
    private String reposUrl;
    @SerializedName("events_url")
    private String eventsUrl;
    @SerializedName("received_events_url")
    private String receivedEventsUrl;


    public Users(String login, String id, String type, String siteAdmin, String avatarUrl, String gravatarId,
                 String url, String htmlUrl, String followersUrl, String followingUrl, String gistsUrl,
                 String starredUrl, String subscriptionsUrl, String organizationsUrl, String reposUrl,
                 String eventsUrl, String receivedEventsUrl) {
        this.login = login;
        this.id = id;
        this.type = type;
        this.siteAdmin = siteAdmin;
        this.avatarUrl = avatarUrl;
        this.gravatarId = gravatarId;
        this.url = url;
        this.htmlUrl = htmlUrl;
        this.followersUrl = followersUrl;
        this.followingUrl = followingUrl;
        this.gistsUrl = gistsUrl;
        this.starredUrl = starredUrl;
        this.subscriptionsUrl = subscriptionsUrl;
        this.organizationsUrl = organizationsUrl;
        this.reposUrl = reposUrl;
        this.eventsUrl = eventsUrl;
        this.receivedEventsUrl = receivedEventsUrl;


    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSiteAdmin() {
        return siteAdmin;
    }

    public void setSiteAdmin(String siteAdmin) {
        this.siteAdmin = siteAdmin;
    }


}
