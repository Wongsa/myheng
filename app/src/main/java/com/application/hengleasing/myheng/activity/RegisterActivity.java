package com.application.hengleasing.myheng.activity;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.application.hengleasing.myheng.R;
import com.application.hengleasing.myheng.functions.DatePickerFragment;
import com.application.hengleasing.myheng.functions.LogOut;

public class RegisterActivity extends AppCompatActivity {

    private EditText editBirthday;
    private EditText editUsername;
    private EditText editCardNumber;
    private EditText editPassword;
    private EditText editPasseordConfirm;

    private Button btnRegis;

    private String username;
    private String password;
    private String passwordComfirm;
    private String birthday;
    private String cardNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initInstances();
    }

    private void initInstances() {
        editBirthday = (EditText) findViewById(R.id.date);
        editUsername = (EditText) findViewById(R.id.editUser);
        editCardNumber = (EditText) findViewById(R.id.editCardNumber);
        editPassword = (EditText) findViewById(R.id.editPass);
        editPasseordConfirm = (EditText) findViewById(R.id.editPassConfirm);

        btnRegis = (Button) findViewById(R.id.btnRegis);
        btnRegis.setOnClickListener(listener);
        editBirthday.setOnClickListener(listener);

        editUsername.addTextChangedListener(textWatcher);
        editCardNumber.addTextChangedListener(textWatcher);
        editPassword.addTextChangedListener(textWatcher);
        editPasseordConfirm.addTextChangedListener(textWatcher);
    }

    //// Event
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (getCurrentFocus() == editUsername) {

                CheckPressSpace(editable, editUsername);

            } else if (getCurrentFocus() == editCardNumber) {

                CheckPressSpace(editable, editCardNumber);
                SetUsernameMaxLenght(editCardNumber);

            } else if (getCurrentFocus() == editPassword) {

                CheckPressSpace(editable, editPassword);

            } else if (getCurrentFocus() == editPasseordConfirm) {

                CheckPressSpace(editable, editPasseordConfirm);
            }

        }
    };

    private void SetUsernameMaxLenght(EditText editText) {
        int cardLenght = 13;
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(cardLenght)});
    }

    private void CheckPressSpace(Editable editable, EditText editText) {
        String result = editable.toString().replaceAll(" ", "");
        if (!editable.toString().equals(result)) {
            editText.setText(result);
            editText.setSelection(result.length());
        }
    }

    //// Click Event
    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnRegis:

                    if (!IsValidate())
                        return;

                    Toast.makeText(RegisterActivity.this, "TODO ", Toast.LENGTH_SHORT).show();
                    //// TO DO Process Regis
                    GetVariable();
                    //AddDataToApi();

                    LogData();

                    break;
                case R.id.date:
                    // show calendar
                    DialogFragment newFragment = new DatePickerFragment();
                    newFragment.show(getFragmentManager(), "Date Picker");
                    break;
            }
        }
    };

    private void LogData() {
        Log.e("dtdt", username);
        Log.e("dtdt", cardNumber);
        Log.e("dtdt", birthday);
        Log.e("dtdt", password);
        Log.e("dtdt", passwordComfirm);

        Toast.makeText(RegisterActivity.this, username + " " + cardNumber + " " + birthday + " "
                + password + " " + passwordComfirm, Toast.LENGTH_SHORT).show();
    }

    public boolean IsValidate() {
        boolean valid = true;

        GetVariable();
//        valid = IsValidateEmpty(valid, username ,editUsername);
//        valid = IsValidateEmpty(valid, password ,editPassword);
//        valid = IsValidateEmpty(valid, passwordComfirm ,editPasseordConfirm);
//        valid = IsValidateEmpty(valid, birthday ,editBirthday);
//        valid = IsValidateEmpty(valid, cardNumber ,editCardNumber);

        if (!IsValidateEmpty(username, editUsername)) {
            if (username.length() < 3) {
                editUsername.setError("กรุณาตั้งชื่อผู้ใช้มากกว่า 3 ตัวอักษร");
                valid = false;
            } else {
                editUsername.setError(null);
            }
        }

        if (!IsValidateEmpty(cardNumber, editCardNumber)) {
            if (cardNumber.length() < 13) {
                editCardNumber.setError("กรุณากรอกรหัสประชาชน 13 หลัก");
                valid = false;
            } else {
                editCardNumber.setError(null);
            }
        }

        if (!IsValidateEmpty(birthday, editBirthday)) {

        }

        if (!password.equals(passwordComfirm)) {
            editPassword.setError("รหัสผ่านไม่ตรงกัน !");
            editPasseordConfirm.setError("รหัสผ่านไม่ตรงกัน !");
            valid = false;
        } else {
            editPassword.setError(null);
            editPasseordConfirm.setError(null);

            valid = IsValidatePassword(valid, password, editPassword);
            valid = IsValidatePassword(valid, passwordComfirm, editPasseordConfirm);
        }

        return valid;
    }


    private void GetVariable() {
        username = editUsername.getText().toString();
        password = editPassword.getText().toString();
        passwordComfirm = editPasseordConfirm.getText().toString();
        birthday = editBirthday.getText().toString();
        cardNumber = editCardNumber.getText().toString();
    }

    private boolean IsValidateEmpty(String text, EditText editText) {
        if (text.isEmpty()) {
            editText.setError("กรุณากรอกข้อมูล !");
            return true;
        } else {
            editText.setError(null);
            return false;
        }
    }

    private static boolean IsValidatePassword(boolean valid, String text, EditText editText) {
        if (text.length() < 4) {
            editText.setError("กรุณากำหนดรหัสผ่านให้มากกว่า 4 ตัวอักษร");
            valid = false;
        } else {
            editText.setError(null);
        }
        return valid;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.Logout:
                LogOut logOut = new LogOut(RegisterActivity.this, RegisterActivity.this);
                logOut.ProcessLogOut();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

