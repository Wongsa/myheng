package com.application.hengleasing.myheng.functions;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by chatchai.ti on 10/8/2560.
 */

public class TokenSession {
    Context context;
    SharedPreferences sharedPerfs;
    SharedPreferences.Editor editor;

    // Prefs Keys
    static String perfsName = "TokenSession";
    static int perfsMode = 0;

    public TokenSession(Context context) {
        this.context = context;
        this.sharedPerfs = this.context.getSharedPreferences(perfsName, perfsMode);
        this.editor = sharedPerfs.edit();
    }

    public void CreateSession(String token) {

        editor.putString("TOKEN", token);

        editor.commit();
    }

    public void DeleteSession() {
        editor.clear();
        editor.commit();
    }

    public String GetToken() {
        return sharedPerfs.getString("TOKEN", null);
    }
}
