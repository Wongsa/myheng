package com.application.hengleasing.myheng.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by chatchai.ti on 2/8/2560.
 */

public class UserResponse {


    @SerializedName("id")
    private String id;
    @SerializedName("CM_CARD_NO")
    private String cardNumber;
    @SerializedName("Username")
    private String username;
    @SerializedName("Password")
    private String password;


    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserResponse(String id, String cardNumber, String username, String password) {
        this.id = id;
        this.cardNumber = cardNumber;
        this.username = username;
        this.password = password;
    }


}
