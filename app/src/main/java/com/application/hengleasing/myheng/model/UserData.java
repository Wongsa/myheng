package com.application.hengleasing.myheng.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chatchai.ti on 10/8/2560.
 */

public class UserData {


    @SerializedName("id")
    private String id;
    @SerializedName("CM_CARD_NO")
    private String cardNumber;
    @SerializedName("CM_PREFIX")
    private String prefixName;
    @SerializedName("CM_F_NAME")
    private String fullName;
    @SerializedName("CM_L_NAME")
    private String lastName;
    @SerializedName("CM_NICKNAME")
    private String nickname;
    @SerializedName("CM_SEX")
    private int gender;
    @SerializedName("CM_SY_AGE")
    private int age;
    @SerializedName("CM_BRITHDAY")
    private String birthday;

    public UserData (String id, String cardNumber, String prefixName, String fullName , String lastName,
                    String nickname,int gender,int age,String birthday  ) {
        this.id = id;
        this.cardNumber = cardNumber;
        this.prefixName = prefixName;
        this.fullName = fullName;
        this.lastName = lastName;
        this.nickname = nickname;
        this.gender = gender;
        this.age = age;
        this.birthday = birthday;
    }


    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPrefixName() {
        return prefixName;
    }

    public void setPrefixName (String prefixName) {
        this.prefixName = prefixName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName (String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }




}
