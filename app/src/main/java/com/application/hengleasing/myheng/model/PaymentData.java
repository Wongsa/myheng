package com.application.hengleasing.myheng.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chatchai.ti on 11/8/2560.
 */

public class PaymentData {

    @SerializedName("id")
    private String id;
    @SerializedName("SY_NO")
    private String contractNumber;
    @SerializedName("SY_MONTH")
    private String contractMonth;
    @SerializedName("SY_DATE")
    private String contractDate;
    @SerializedName("SY_MTH_ATM")
    private String cantractPrice;
    @SerializedName("SLIP_DATE")
    private String slipDate;
    @SerializedName("SLIP_AMT")
    private String slipAmout;

    public PaymentData(String id, String contractMonth, String contractNumber , String cantractPrice,
                       String contractDate , String slipDate , String slipAmout) {
        this.id = id;
        this.contractMonth = contractMonth;
        this.contractNumber = contractNumber;
        this.cantractPrice = cantractPrice;
        this.contractDate = contractDate;
        this.slipDate = slipDate;
        this.slipAmout = slipAmout;
    }

    public String getCantractPrice() {
        return cantractPrice;
    }

    public void setCantractPrice(String cantractPrice) {
        this.cantractPrice = cantractPrice;
    }

    public String getContractDate() {
        return contractDate;
    }

    public void setContractDate(String contractDate) {
        this.contractDate = contractDate;
    }

    public String getSlipDate() {
        return slipDate;
    }

    public void setSlipDate(String slipDate) {
        this.slipDate = slipDate;
    }

    public String getSlipAmout() {
        return slipAmout;
    }

    public void setSlipAmout(String slipAmout) {
        this.slipAmout = slipAmout;
    }

    public String getContractMonth() {
        return contractMonth;
    }

    public void setContractMonth(String contractMonth) {
        this.contractMonth = contractMonth;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String username) {
        this.contractNumber = contractNumber;
    }
}
