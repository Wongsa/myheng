package com.application.hengleasing.myheng.functions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.application.hengleasing.myheng.activity.LoginActivity;

/**
 * Created by chatchai.ti on 9/8/2560.
 */

public class LogOut extends AppCompatActivity {

    protected Context context;
    protected Activity activity;

    public LogOut(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    //@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void ProcessLogOut() {
        TokenSession tokenSession = new TokenSession(context);
        tokenSession.DeleteSession();

        Intent intent = new Intent(context, LoginActivity.class);
        // Closing all the Activities
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        // Add new Flag to start new Activity
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        activity.finish();

    }

    /// TODO Dialog Logout


}
