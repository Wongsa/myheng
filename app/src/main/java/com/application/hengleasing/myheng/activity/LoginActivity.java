package com.application.hengleasing.myheng.activity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.application.hengleasing.myheng.R;
import com.application.hengleasing.myheng.api.ApiClient;
import com.application.hengleasing.myheng.api.ApiInterface;

import com.application.hengleasing.myheng.functions.TokenSession;
import com.application.hengleasing.myheng.model.UserResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity {

    private EditText editUser;
    private EditText editPass;
    private Button btnLogin;
    private TextView tvRegis;

    private String username;
    private String password;
    private String token;
    private String cardNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initInstances();

    }

    private void initInstances() {
        editUser = (EditText) findViewById(R.id.editUser);
        editPass = (EditText) findViewById(R.id.editPass);
        btnLogin = (Button) findViewById(R.id.btnLogIn);
        tvRegis = (TextView) findViewById(R.id.tvRegit);

        btnLogin.setOnClickListener(listener);
        tvRegis.setOnClickListener(listener);
        //EnableBottomLogin();


        editUser.addTextChangedListener(textWatcher);
        editPass.addTextChangedListener(textWatcher);

    }

    ////// username and pass keypress
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            //EnableBottomLogin();
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (getCurrentFocus() == editUser) {

                CheckPressSpace(editable, editUser);

            } else if (getCurrentFocus() == editPass) {

                CheckPressSpace(editable, editPass);
            }
            // Toast.makeText(LoginActivity.this,getCurrentFocus().toString(),Toast.LENGTH_SHORT ).show();

        }
    };

    private void CheckPressSpace(Editable editable, EditText editText) {
        String result = editable.toString().replaceAll(" ", "");
        if (!editable.toString().equals(result)) {
            editText.setText(result);
            editText.setSelection(result.length());
        }
    }


    private void EnableBottomLogin() {
        GetUsernameAndPassword();
        if (!isUserLogInEmpty()) {
            btnLogin.setEnabled(true);
        } else {
            btnLogin.setEnabled(false);
        }
        //Toast.makeText(LoginActivity.this, isUserLogInEmpty() + "", Toast.LENGTH_SHORT).show();
    }


    private boolean isUserLogInEmpty() {
        if (username.trim().equals("") || password.trim().equals("")) {
            return true;
        }
        return false;
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnLogIn:

                    GetUsernameAndPassword();

                    boolean IsUserEmpty = IsValidateEmpty(username, editUser);
                    boolean IsPassEmtpy = IsValidateEmpty(password, editPass);

                    if (IsPassEmtpy || IsUserEmpty)
                        return;

                    LoadingProgress();
                    btnLogin.setEnabled(false);
                    break;
                case R.id.tvRegit:

                    startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                    break;

            }
        }


    };

    private boolean IsValidateEmpty(String text, EditText editText) {
        if (text.isEmpty()) {
            editText.setError("กรุณากรอกข้อมูล !");
            return true;
        } else {
            editText.setError(null);
            return false;
        }
    }


    private void LogIn(final String token,final  String cardNumber) {
        if (!IsUserLogInPass(token)) {
            LogInFailed();
            btnLogin.setEnabled(true);
            return;
        }
        SentVariableToNextAct(token,cardNumber);


    }

    private void LoadingProgress() {
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("กำลังเข้าสู้ระบบ...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        CallApi();

                        progressDialog.dismiss();
                    }
                }, 1500);
    }

    private void SentVariableToNextAct(String token,String cardNumber) {
        TokenSession tokenSession = new TokenSession(LoginActivity.this);
        tokenSession.CreateSession(token);

        Intent intent = new Intent(LoginActivity.this, MyListActivity.class);
        intent.putExtra("CardNumber",cardNumber);
        startActivity(intent);
        finish();
    }

    private void CallApi() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        //Call <UserResponse> call = apiService.getStatusLogIn(username,password);

        Call<List<UserResponse>> call = apiService.getTokenLogIn(username, password);

        call.enqueue(new Callback<List<UserResponse>>() {

            @Override
            public void onResponse(Call<List<UserResponse>> call, Response<List<UserResponse>> response) {
                //UserResponse userResponse = response.body();
                List<UserResponse> userResponse = response.body();

               // Toast.makeText(LoginActivity.this, "Success" + response.code(), Toast.LENGTH_SHORT).show();

                if (!response.isSuccessful() || userResponse.isEmpty()) {
                    token = "";
                    cardNumber = "";
                    LogIn(token,cardNumber);
                    Toast.makeText(LoginActivity.this, "No" + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    for (int i = 0; i < userResponse.size(); i++) {
                        Log.e("aaaa", userResponse.get(i).getId() + " : " + userResponse.get(i).getCardNumber()
                                + " : " + userResponse.get(i).getUsername() + " : " + userResponse.get(i).getPassword());
                        token = userResponse.get(i).getCardNumber();
                        cardNumber = userResponse.get(i).getCardNumber();

//                        Toast.makeText(LoginActivity.this, "Success" + userResponse.get(i).getId() + " : " + userResponse.get(i).getCardNumber()
//                                + " : " + userResponse.get(i).getUsername() + " : " + userResponse.get(i).getPassword(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                    Log.e("onResponse", "There is an error" + e);
                    Toast.makeText(LoginActivity.this, "Error" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                //Toast.makeText(LoginActivity.this, "Success" + response.code() + token, Toast.LENGTH_SHORT).show();
                LogIn(token,cardNumber);
            }

            @Override
            public void onFailure(Call<List<UserResponse>> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "API Fail" + t.toString(), Toast.LENGTH_SHORT).show();
                Log.e("api", "FFFF" + t.toString());
            }
        });


    }

    private void LogInFailed() {
        //Toast.makeText(LoginActivity.this, "กรุณาตรวจสอบชื่อผู้ใช้และรหัสผ่านอีกครั้ง !", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
        builder1.setMessage("กรุณาตรวจสอบชื่อผู้ใช้และรหัสผ่านอีกครั้ง !");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "ตกลง",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder1.setTitle("แจ้งเตือน").setIcon(R.drawable.caution_sign);

        AlertDialog alert11 = builder1.create();
        alert11.show();

        editUser.requestFocus();
        editUser.setError("กรุณากรอกใหม่");
        editPass.setError("กรุณากรอกใหม่");
        editPass.setText(null);
    }

    private void GetUsernameAndPassword() {
        username = editUser.getText().toString();
        password = editPass.getText().toString();
    }

    private boolean IsUserLogInPass(String token) {

        //token = "";
        if (!token.isEmpty()) {
            return true;
        }
        return false;
    }


}
