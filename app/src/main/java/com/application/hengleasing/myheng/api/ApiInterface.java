package com.application.hengleasing.myheng.api;

import com.application.hengleasing.myheng.model.ContractData;
import com.application.hengleasing.myheng.model.PaymentData;
import com.application.hengleasing.myheng.model.UserData;
import com.application.hengleasing.myheng.model.UserResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiInterface {

    @GET("customeruser")
    Call<List<UserResponse>> getTokenLogIn(@Query("Username") String username, @Query("Password") String password);

    @GET("login")
    Call<List<UserData>> getUserData(@Query("CM_CARD_NO") String cardNumber);

    @GET("getSY")
    Call<List<ContractData>> getContractData(@Query("CM_CARD_NO") String cardNumber);

    @GET("SYDetail")
    Call<List<PaymentData>> getPaymentData(@Query("SY_NO") String contractNumber);
}
